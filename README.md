# rosemary-user 11 RP1A.200720.011 V12.5.19.0.RKLINXM release-keys
- manufacturer: xiaomi
- platform: mt6785
- codename: maltose
- flavor: maltose-user
rosemary-user
rosemary-user
rosemary-user
secret-user
secret-user
- release: 11
- id: RP1A.200720.011
- incremental: V12.5.19.0.RKLINXM
- tags: release-keys
- fingerprint: Redmi/maltose_in/maltose:11/RP1A.200720.011/V12.5.19.0.RKLINXM:user/release-keys
Redmi/rosemary/rosemary:11/RP1A.200720.011/V12.5.19.0.RKLINXM:user/release-keys
POCO/rosemary_p_in/rosemary:11/RP1A.200720.011/V12.5.19.0.RKLINXM:user/release-keys
Redmi/rosemary_in/rosemary:11/RP1A.200720.011/V12.5.19.0.RKLINXM:user/release-keys
Redmi/secret_in/secret:11/RP1A.200720.011/V12.5.19.0.RKLINXM:user/release-keys
Redmi/secret_in2/secret:11/RP1A.200720.011/V12.5.19.0.RKLINXM:user/release-keys
- is_ab: true
- brand: Redmi
- branch: rosemary-user-11-RP1A.200720.011-V12.5.19.0.RKLINXM-release-keys
- repo: redmi_maltose_dump
